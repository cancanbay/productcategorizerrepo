package Util;

public class Constants {
    public static final String FIRST_INPUT_XML_FILE = "products1.xml";
    public static final String SECOND_INPUT_XML_FILE = "products2.xml";
    public static final String THIRD_INPUT_XML_FILE = "products3.xml";
    public static final String PRODUCT = "Product";
    public static final String NAME = "Name";
    public static final String PRICE = "Price";
    public static final String CURRENCY = "Currency";
    public static final String BESTBEFORE = "BestBefore";
    public static final String DATE = "Date";
    public static final String ITEM = "Item";
    public static final String ID = "Id";
    public static final String API_URL = "http://data.fixer.io/api/latest?access_key=26716464ac28589a3cd83f568d15759a";
}
