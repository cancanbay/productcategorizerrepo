package Service;

import XmlModel.Product;
import XmlModel.Response;
import Util.Constants;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SecondCatalogXmlParser implements XmlParser {

    @Override
    public Response parse(File inputFile) throws SAXException, IOException, ParserConfigurationException {
        NodeList nodeList = getNodeList(inputFile,Constants.PRODUCT);
        List<Product> products = new ArrayList<Product>();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node nNode = nodeList.item(i);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) nNode;
                Product product = new Product();
                product.setName(element.getAttribute(Constants.NAME));
                product.setPrice(Double.parseDouble(element.getElementsByTagName(Constants.PRICE).item(0).getTextContent().equals("")
                        ? "0.0" : element.getElementsByTagName(Constants.PRICE).item(0).getTextContent()));
                product.setCurrency(element.getElementsByTagName(Constants.PRICE).item(0).getAttributes().getNamedItem(Constants.CURRENCY).getTextContent());
                product.setBestBefore(element.getElementsByTagName(Constants.BESTBEFORE).item(0).getAttributes().getNamedItem(Constants.DATE).getTextContent());
                products.add(product);
            }
        }
        Response response = new Response();
        response.setProducts(products);
        return response;
    }
}
