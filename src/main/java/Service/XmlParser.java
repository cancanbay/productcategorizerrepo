package Service;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

public interface XmlParser<T> {

    default NodeList getNodeList(File inputFile, String xmlRootElement) throws SAXException,IOException,ParserConfigurationException{
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document document = db.parse(inputFile);
        document.getDocumentElement().normalize();
        return document.getElementsByTagName(xmlRootElement);
    }
    T parse(File inputFile) throws SAXException,IOException,ParserConfigurationException;
}
