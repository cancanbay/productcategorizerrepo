package Service;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


public class FixerAPIService {
    public static JSONObject sendHttpRequest(String url) throws Exception {
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("GET");
        JSONObject jsonObj = null;
        if(con.getResponseCode() == HttpURLConnection.HTTP_OK){
            System.out.println("Success!");
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            jsonObj = new JSONObject(response.toString());
            System.out.println(jsonObj.getJSONObject("rates"));
        }
        else{
            System.out.println("Api responded with Http Status Code of "+ con.getResponseCode());
        }
      return jsonObj.getJSONObject("rates");
    }
}
