package Service;

import XmlModel.Item;
import XmlModel.Product;
import XmlModel.Response;
import Util.Constants;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ThirdCatalogXmlParser implements XmlParser {
    @Override
    public Response parse(File inputFile) throws SAXException, IOException, ParserConfigurationException {
       NodeList nodeList = getNodeList(inputFile,Constants.ITEM);
        List<Item> items = new ArrayList<>();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node nNode = nodeList.item(i);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) nNode;
                Item item = new Item();
                item.setId(element.getElementsByTagName(Constants.ID).item(0).getTextContent());
                Product product = new Product();
                product.setId(element.getElementsByTagName(Constants.ID).item(0).getTextContent());
                product.setName(element.getElementsByTagName(Constants.NAME).item(0).getTextContent());
                product.setPrice(Double.parseDouble(element.getElementsByTagName(Constants.PRICE).item(0).getTextContent().matches("[a-zA-Z]+")
                        ? "0.0" : element.getElementsByTagName(Constants.PRICE).item(0).getTextContent()));
                product.setCurrency(element.getElementsByTagName(Constants.CURRENCY).item(0).getTextContent());
                product.setBestBefore(element.getElementsByTagName(Constants.PRODUCT).item(0).getAttributes().getNamedItem(Constants.BESTBEFORE).getTextContent());
                item.setProduct(product);
                items.add(item);
            }
        }
        Response response = new Response();
        response.setItems(items);
        return response;
    }
}
