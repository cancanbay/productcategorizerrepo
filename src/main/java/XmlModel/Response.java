package XmlModel;

import java.util.List;

public class Response {
    private List<Item> items;
    private List<Product> products;

    public Response() {
        super();
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    @Override
    public String toString() {
        return "Response{" +
                "items=" + items +
                ", products=" + products +
                '}';
    }
}
