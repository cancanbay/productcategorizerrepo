import Service.*;
import XmlModel.Item;
import Util.Constants;
import XmlModel.Product;
import XmlModel.Response;
import com.google.gson.Gson;
import org.json.JSONObject;
import org.xml.sax.SAXException;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) throws Exception {
        File firstInputFile = new File(Constants.FIRST_INPUT_XML_FILE);
        File secondInputFile = new File(Constants.SECOND_INPUT_XML_FILE);
        File thirdInputFile = new File(Constants.THIRD_INPUT_XML_FILE);
        XmlParser firstCatalogParser = new FirstCatalogXmlParser();
        XmlParser secondCatalogParser = new SecondCatalogXmlParser();
        XmlParser thirdCatalogParser = new ThirdCatalogXmlParser();
        Response firstResponse = (Response) firstCatalogParser.parse(firstInputFile);
        Response secondResponse = (Response) secondCatalogParser.parse(secondInputFile);
        Response thirdResponse  = (Response) thirdCatalogParser.parse(thirdInputFile);
        List<Product> allCatalogProducts = new ArrayList<>();
        JSONObject jsonResponse = FixerAPIService.sendHttpRequest(Constants.API_URL);
        if(jsonResponse != null){
            addExchangeConvertedResponse(firstResponse, allCatalogProducts, jsonResponse);
            addExchangeConvertedResponse(secondResponse, allCatalogProducts, jsonResponse);
            for(Item item : thirdResponse.getItems()){
                if(!item.getProduct().getCurrency().equals("EUR") && !item.getProduct().getCurrency().equals("")){
                    //primitive double returns 0 if null
                    if(jsonResponse.getDouble(item.getProduct().getCurrency()) !=  0){
                        item.getProduct().setPrice(item.getProduct().getPrice()*jsonResponse.getDouble(item.getProduct().getCurrency()));
                    }
                }
                allCatalogProducts.add(item.getProduct());
            }
        }
        for(Product product : allCatalogProducts){
            System.out.println(product);
        }
    }

    private static void addExchangeConvertedResponse(Response firstResponse, List<Product> allCatalogProducts, JSONObject jsonResponse) {
        for(Product product : firstResponse.getProducts()){
            if(!product.getCurrency().equals("EUR") && !product.getCurrency().equals("")){
                if(jsonResponse.getDouble(product.getCurrency()) > 0){
                    product.setPrice(product.getPrice()*jsonResponse.getDouble(product.getCurrency()));
                }
            }
            allCatalogProducts.add(product);
        }
    }
}
